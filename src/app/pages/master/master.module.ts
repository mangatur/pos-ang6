import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { CategoryComponent } from './category/category.component';
// import { VariantComponent } from './variant/variant.component';
// import { ProductComponent } from './product/product.component';
import { CategoryDetailComponent } from './category-detail/category-detail.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    // CategoryComponent,
    // VariantComponent,
    // ProductComponent,
    CategoryDetailComponent
  ],
  exports: [
    CategoryDetailComponent
  ]
})
export class MasterModule { }
