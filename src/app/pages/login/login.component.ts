import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppComponent } from '../../app.component';

import { AuthService } from '../../services/auth.service';
import { User } from '../../model/user';

import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit, OnInit {
  user: User = new User();
  returnUrl: String = '';
  errorNotification: String = '';
  constructor(
    private authService: AuthService,
    private appComponent: AppComponent,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    console.log('ngOnInit: Logging out!!!');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit: Logging out!!!');
  }

  Login() {
    this.authService.postAuth(this.user)
      .subscribe((data: any) => {
        console.log(data);
        localStorage.setItem('userName', data.userName);
        localStorage.setItem('userToken', data.token);
        this.appComponent.loggedIn = true;
        this.appComponent.logText = 'Logout!';
        this.appComponent.user = {
          userName : data.userName,
          password : ''
        };
        this.router.navigate([this.returnUrl]);
      },
        (err) => {
          console.log(err.error);
          this.errorNotification = err.error.message;
        },
        () => console.log('Login successful!'));
  }
}
