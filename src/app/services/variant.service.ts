import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../base/config';
import { Variant } from '../model/variant';

@Injectable({
  providedIn: 'root'
})
export class VariantService {
  variants: Variant[];
  variant: Variant;
  constructor(private http: HttpClient) { }

  get() {
    return this.http.get(Config.Url + '/api/variant', {
      headers: new HttpHeaders({ 'x-access-token': localStorage.getItem('userToken') })
    });
  }
}
